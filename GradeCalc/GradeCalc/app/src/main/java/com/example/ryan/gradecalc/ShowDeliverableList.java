package com.example.ryan.gradecalc;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.example.ryan.gradecalc.dao.DeliverableDAO;
import com.example.ryan.gradecalc.model.Deliverable;

import java.util.ArrayList;

/**
 * Created by krystofurr on 01/02/16.
 *
 * This will show all deliverables created in one list.  Click on them to provide
 * a menu to either delete or update the deliverable.  Press submit to continue to entering
 * the grades
 *
 */
public class ShowDeliverableList extends AppCompatActivity {

    private DeliverableDAO deliverableDAO;
    private CursorAdapter cursorAdapter;
    private Cursor cursor;
    private ArrayList<Deliverable> deliverables = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_deliverable_list);

        // PULL DELIVERABLES FROM THE DATABASE HERE
        deliverableDAO = new DeliverableDAO(this, null);
        cursor = deliverableDAO.getDeliverablesCursor();

        // POPULATE THE DELIVERABLES INTO THE LISTVIEW VIA THE ARRAYADAPTER

        ListView listView = (ListView) findViewById(R.id.listview_deliverableList);

        // USING ARRAY ADAPTER ( CAN USE THIS BUT SHOULDN'T )
//        listAdapter = new ArrayAdapter<Deliverable>(this, android.R.layout.simple_list_item_1, deliverables);
//
//         // Set the ArrayAdapter as the ListView's adapter.
//        listView.setAdapter( listAdapter );

        // USING CURSOR ADAPTER ( PROPER WAY OF CALLING DATA FROM DATABASE )
        cursorAdapter = new SimpleCursorAdapter(this,
                R.layout.listview_custom,
                cursor,
                new String[]{"name", "weight"},
                new int[]{R.id.deliverable_name, R.id.deliverable_weight},
                0);

        listView.setAdapter( cursorAdapter );


        // LISTENER - SUBMIT BUTTON
        Button submitButton = (Button) findViewById(R.id.submitButton2);
        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Log.d("Submit", "Submit button was clicked.");

                Intent intent = new Intent(ShowDeliverableList.this, ShowDeliverables.class);
                intent.putExtra("deliverableCounter", cursor.getCount());
                startActivity(intent);
                finish();
            }
        });

        // LISTENER - LIST VIEW TO DISPLAY A POPUP FOR EITHER 'UPDATE' OR 'DELETE'

        // ITEM LISTENER - Define a listener for the ListView ( Because I cannot extend 'ListActivity' )
        // REF: http://stackoverflow.com/questions/16389581/android-create-a-popup-that-has-multiple-selection-options
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> listView, View itemView, int position, long id) {

                // DELIVERABLE 'ID' HAS TO BE MADE FINAL IN ORDER FOR THE ALERT DIALOG TO SEE IT
                Log.w("ID OF THE ITEM CLICKED",": " + id);
                final int finalId = (int) id;

                String options[] = new String[] {"Add", "Update", "Delete"};

                AlertDialog.Builder builder = new AlertDialog.Builder(ShowDeliverableList.this);
                builder.setTitle("What would you like to do with this deliverable?");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent;
                        switch(which) {
                            // ADD
                            case 0:
                                Log.w("ADD", "Add");
                                intent = new Intent(ShowDeliverableList.this, AddDeliverable.class);
                                startActivity(intent);
                                finish();
                                break;
                            // UPDATE
                            case 1:
                                Log.w("UPDATE", "Update");
                                intent = new Intent(ShowDeliverableList.this, UpdateDeliverable.class);
                                Log.d("Delivearble", finalId+"");
                                intent.putExtra("deliverable_id", finalId);
                                startActivity(intent);
                                finish();
                                break;
                            // DELETE
                            case 2:
                                Log.w("DELETE", "Delete");
                                deliverableDAO.deleteDeliverable(finalId);
                                // Will update the list once deleted
                                cursor = deliverableDAO.getDeliverablesCursor();
                                cursorAdapter.changeCursor(cursor);
                                break;

                        }
                    }
                });
                builder.show();

            }
        };

        listView.setOnItemClickListener(itemClickListener);

    }


    // IMPORTANT! - DO NOT CLOSE THE CURSOR WHEN YOU ARE WORKING WITH THE CURSOR DURING THE ACTIVITY! ONLY ON onDestory
    @Override
    public void onDestroy() {
        super.onDestroy();

        deliverableDAO.closeConnection();

    } // END: onDestroy()


}
