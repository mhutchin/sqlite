package com.example.ryan.gradecalc;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ryan.gradecalc.dao.DeliverableDAO;
import com.example.ryan.gradecalc.model.Deliverable;

import java.util.ArrayList;

public class ShowDeliverables extends AppCompatActivity {

    private TextView txtDeliverableName;
    private TextView txtDeliverableWeight;
    private EditText deliverableGrade;
    private int deliverableCounter;
    private Button submitButton;

    private ArrayList deliverables;
    SQLiteDatabase db;
    private DeliverableDAO deliverableDAO;

    double subtotal = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_deliverables);

        deliverableDAO = new DeliverableDAO(this, null);

        submitButton = (Button) findViewById(R.id.submitButton);
        txtDeliverableName = (TextView) findViewById(R.id.txtDeliverableName);
        txtDeliverableWeight = (TextView) findViewById(R.id.txtdeliverableWeight);
        deliverableGrade = (EditText) findViewById(R.id.deliverableGrade);

        deliverableCounter = getIntent().getIntExtra("deliverableCounter", 0);


        Cursor results = deliverableDAO.getDeliverablesCursor();

        Log.d("RESULTS", "Results count = "+results.getCount()+" DeliverableCounter = "+deliverableCounter);
        results.moveToPosition(results.getCount() - deliverableCounter);

        deliverableCounter--;

        txtDeliverableName.setText(results.getString(1));
        txtDeliverableWeight.setText(results.getString(2));

        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Log.d("Submit", "Submit button was clicked.");
                deliverableGrade = (EditText) findViewById(R.id.deliverableGrade);

                // DO CALCULATION
                /*
                    WEIGHT / 100 * Grade = .decimal answer
                    Add all .decimal answers and multiply by 100

                 */
                double weight = Double.parseDouble(txtDeliverableWeight.getText().toString());
                double grade = Double.parseDouble(deliverableGrade.getText().toString());

                subtotal += (weight / 100) * (grade / 100);

                //db.execSQL("UPDATE deliverables SET name='" + txtDeliverableName.getText().toString() + "', weight='" + txtDeliverableWeight.getText().toString() + "', grade='" + deliverableGrade.getText().toString() + "' WHERE name = '" + txtDeliverableName.getText().toString() + "' AND " + "weight = '" + txtDeliverableWeight.getText().toString() + "';");

                Cursor result = deliverableDAO.getDeliverablesCursor();
                result.moveToFirst();
                Log.d("RESULTS", "GRADE = " + result.getString(2));

                if(deliverableCounter <= 0){

                    double average = subtotal * 100;
                    Intent intent = new Intent(ShowDeliverables.this, ShowAverage.class);
                    intent.putExtra("average", average);

                    startActivity(intent);
                    finish();
                }
                else{
                    Intent intent = new Intent(ShowDeliverables.this, ShowDeliverables.class);
                    intent.putExtra("deliverableCounter", deliverableCounter);

                    startActivity(intent);

                    finish();
                }
            }
        });

    }
}
