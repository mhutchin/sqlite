package com.example.ryan.gradecalc;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.ryan.gradecalc.dao.DeliverableDAO;

public class ShowAverage extends AppCompatActivity {

    private TextView txtAverage;
    private double average;
    private int counter;
    private Button button;

    private DeliverableDAO deliverableDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_average);

        average = getIntent().getDoubleExtra("average", 0.0);
        button = (Button) findViewById(R.id.button);
        txtAverage = (TextView) findViewById(R.id.txtAverage);

        Log.d("Average", average+"");
        txtAverage.setText(String.valueOf(average));

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Log.d("Submit", "Submit button was clicked.");

                Intent intent = new Intent(ShowAverage.this, AddDeliverable.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
