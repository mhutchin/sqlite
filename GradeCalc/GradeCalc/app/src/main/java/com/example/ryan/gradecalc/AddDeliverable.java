package com.example.ryan.gradecalc;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.example.ryan.gradecalc.dao.DeliverableDAO;
import com.example.ryan.gradecalc.model.Deliverable;

import java.util.ArrayList;

public class AddDeliverable extends AppCompatActivity {

    private final static double MAX_WEIGHT_DELIVERABLE = 100;
    private EditText deliverableName;
    private EditText deliverableWeight;
    private Button submitButton;
    private double totalWeight;
    private TextView txtTotalWeight;
    private int deliverableCounter = 0;

    DeliverableDAO deliverableDAO;
    private ArrayList<Deliverable> deliverables = new ArrayList<Deliverable>();
    private ListView listView;
    private CursorAdapter cursorAdapter;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_deliverable);

        // CHECK TO SEE WHAT DELIVERABLES ARE CURRENTLY IN THE DATABASE...
        deliverableDAO = new DeliverableDAO(this, null);
        deliverables = deliverableDAO.getDeliverables();

        for(Deliverable deliverable : deliverables) {
            totalWeight += deliverable.getWeight();
        }



        Log.d("TOTAL WEIGHT", totalWeight+"");
        if(totalWeight < MAX_WEIGHT_DELIVERABLE) {

            submitButton = (Button) findViewById(R.id.submitButton);
            txtTotalWeight = (TextView) findViewById(R.id.txtTotalWeight);
            txtTotalWeight.setText(String.valueOf(totalWeight));
            totalWeight = getIntent().getDoubleExtra("totalWeight", 0);
            deliverableCounter = getIntent().getIntExtra("deliverableCounter", 0);



            //deleteDatabase("GradeCalc");
//        db = openOrCreateDatabase("GradeCalc", MODE_PRIVATE, null);
//        if(db.isOpen()){
//            Log.d("DATABASE", "DATABASE IS OPEN");
//        }
//        db.execSQL("CREATE TABLE IF NOT EXISTS deliverables(name VARCHAR, weight DOUBLE, grade DOUBLE);");


            if(totalWeight >= 100){
                Intent intent = new Intent(AddDeliverable.this, ShowDeliverableList.class);
//                intent.putExtra("deliverableCounter", deliverableCounter);
                startActivity(intent);
                finish();
            }



            submitButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Log.d("Submit", "Submit button was clicked.");
                    deliverableName = (EditText) findViewById(R.id.txtDeliverableName);
                    deliverableWeight = (EditText) findViewById(R.id.deliverableWeight);
                    deliverableCounter++;
                    String deliverableNameString = deliverableName.getText().toString();
                    Double deliverableWeightDouble = Double.parseDouble(deliverableWeight.getText().toString());

                    deliverableDAO.insertDeliverable(deliverableNameString, deliverableWeightDouble);
//                db.execSQL("INSERT INTO deliverables VALUES ('" + deliverableName.getText() + "','" + Double.parseDouble(deliverableWeight.getText().toString())+"','0');");

                    Intent intent = new Intent(AddDeliverable.this, AddDeliverable.class);
                    intent.putExtra("totalWeight", totalWeight + deliverableWeightDouble);
                    intent.putExtra("deliverableCounter", deliverableCounter);

                    startActivity(intent);
                    finish();
                }
            });

        // ELSE - It's equal to or greater than MAX
        } else {

            Intent intent = new Intent(AddDeliverable.this, ShowDeliverableList.class);
//            intent.putExtra("deliverableCounter", deliverableCounter);
            startActivity(intent);
            finish();

        }




    }

    // IMPORTANT! - DO NOT CLOSE THE CURSOR WHEN YOU ARE WORKING WITH THE CURSOR DURING THE ACTIVITY! ONLY ON onDestory
    @Override
    public void onDestroy() {
        super.onDestroy();

        deliverableDAO.closeConnection();

    } // END: onDestroy()
}
