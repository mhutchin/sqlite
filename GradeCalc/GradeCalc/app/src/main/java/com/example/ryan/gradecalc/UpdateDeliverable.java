package com.example.ryan.gradecalc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.ryan.gradecalc.dao.DeliverableDAO;
import com.example.ryan.gradecalc.model.Deliverable;

/**
 * Created by krystofurr on 02/02/16.
 *
 * Used to update the values within a deliverable
 *
 */
public class UpdateDeliverable extends AppCompatActivity {

    DeliverableDAO deliverableDAO;
    private String deliverableId;        // Initialized in constructor to use with listener


    public UpdateDeliverable() {
        //Log.d("Delivearble", getIntent().getStringExtra("deliverable_id"));
        try{
            this.deliverableId = getIntent().getIntExtra("deliverable_id", 0)+"";
            //Log.d("Delivearble", getIntent().getIntExtra("deliverable_id",0)+"");
        }
        catch(NullPointerException e){
            Log.d("Delivearble_ID", "NULL POINTER");
            //Log.d("Delivearble", deliverableId);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_deliverable);

        deliverableDAO = new DeliverableDAO(this, null);

        // Grab the intent
        //deliverableId = Integer.parseInt(getIntent().getStringExtra("deliverableId"));


        // Grab all the views for the page
        Button buttonSave = (Button) findViewById(R.id.button_update_deliverable);
        TextView textViewName = (TextView) findViewById(R.id.textview_update_deliverableName);
        final EditText editTextName = (EditText) findViewById(R.id.edittext_update_deliverableName);
        TextView textViewWeight = (TextView) findViewById(R.id.textview_update_deliverableWeight);
        final EditText editTextWeight = (EditText) findViewById(R.id.edittext_update_deliverableWeight);

        // Populate the text boxes with the deliverable data
        Deliverable deliverable = deliverableDAO.getDeliverable(Integer.parseInt(deliverableId));
        editTextName.setText(deliverable.getName());
        editTextWeight.setText(String.valueOf(deliverable.getWeight()));

        // Set some text for the view
        textViewName.setText("Deliverable Name:");
        textViewWeight.setText("Deliverable Weight:");





        // LISTENER - Button
        buttonSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Log.d("UPDATE", "Save was clicked");

                deliverableDAO.updateDeliverable(Integer.parseInt(deliverableId), editTextName.getText().toString(), Double.parseDouble(editTextWeight.getText().toString()));
//                db.execSQL("INSERT INTO deliverables VALUES ('" + deliverableName.getText() + "','" + Double.parseDouble(deliverableWeight.getText().toString())+"','0');");

                Intent intent = new Intent(UpdateDeliverable.this, ShowDeliverableList.class);
//                intent.putExtra("totalWeight", totalWeight + deliverableWeightDouble);
//                intent.putExtra("deliverableCounter", deliverableCounter);

                startActivity(intent);
                finish();
            }
        });

    }



}
