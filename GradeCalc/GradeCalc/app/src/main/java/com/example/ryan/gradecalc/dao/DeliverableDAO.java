package com.example.ryan.gradecalc.dao;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.example.ryan.gradecalc.model.Deliverable;

import java.util.ArrayList;

/**
 * Created by krystofurr on 01/02/16.
 *
 *  This is the 'Programmatic' ( and recommended ) way of creating a database with Android
 *  REF: http://developer.android.com/guide/topics/data/data-storage.html#db
 *
 *  Good reference for this class
 *  http://www.tutorialspoint.com/android/android_sqlite_database.htm
 *
 */
public class DeliverableDAO extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "GradeCalc";          // <=== Set to 'null' for an in-memory database
    public static final String TABLE_NAME = "Deliverables";
    public static final String COLUMN_ID = "_id";           // <=== This is required
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WEIGHT = "weight";

    private static final int DB_VERSION = 1;                 // the version of the database

    private static final String SQL_CREATE_TABLE_DELIVERABLES = "CREATE TABLE " + TABLE_NAME + "( " +
                                                                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                                                COLUMN_NAME + " TEXT, " +
                                                                COLUMN_WEIGHT + " DOUBLE);";

    private SQLiteDatabase db;
    private Cursor cursor;


    public DeliverableDAO(Context context, SQLiteDatabase.CursorFactory factory, DatabaseErrorHandler errorHandler) {
        super(context, DATABASE_NAME, factory, DB_VERSION, errorHandler);
    }

    public DeliverableDAO(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, DATABASE_NAME, factory, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // Create the database
        db.execSQL(SQL_CREATE_TABLE_DELIVERABLES);
        // Insert a row into 'Deliverables'.  _id autoincrements so you do not need to add it
//        insertDeliverable(db, "Assignments", 30);
//        insertDeliverable(db, "Midterm", 30);
//        insertDeliverable(db, "Final", 40);

    }

    /**
     *  onUpgrade will execute if the DB_VERSION value is changed.  Android will compare the
     *  new value of version with the old value of version.  Based on that it will call the
     * 'onUpgrade' method
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    // http://www.vogella.com/tutorials/AndroidSQLite/article.html
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

//        if(oldVersion == 1) {
//            Log.i(CanesCampDAO.class.getName(), "Database version is at 1");
//        } else {
//
//            Log.i(CanesCampDAO.class.getName(),
//                    "Upgrading database from version " + oldVersion + " to "
//                            + newVersion + ", which will destroy all old data");
//            db.execSQL("DROP TABLE IF EXISTS ");
//            onCreate(db);
//        }
    }

    /**
     *
     * Helper method to insert a row into the 'Deliverables' table
     *
     */
    public boolean insertDeliverable(String name, double weight) {
        boolean success;

        try {
            db = this.getWritableDatabase();
            ContentValues deliverableValues = new ContentValues();

            // NAME, WEIGHT
            deliverableValues.put("name", name);
            deliverableValues.put("weight", weight);


            db.insert(TABLE_NAME, null, deliverableValues);

            success = true;
        } catch ( SQLiteException ex ) {
            Log.d("DeliverableDAO", "Failed to insert into the database");
            success = false;
        }

        return success;
    }

    public boolean updateDeliverable(int id, String name, double weight) {
        boolean success;

        try {
            db = this.getWritableDatabase();
            ContentValues deliverableValues = new ContentValues();

            // NAME, WEIGHT
            deliverableValues.put("name", name);
            deliverableValues.put("weight", weight);


            db.update(TABLE_NAME, deliverableValues, COLUMN_ID + "=" + id, null);

            success = true;
        } catch( SQLiteException ex ) {
            Log.d("DeliverableDAO", "Failed to update the record");
            success = false;
        }

        return success;

    }


    public boolean deleteDeliverable(int id) {
        boolean success;

        try {
            db = this.getWritableDatabase();
            db.delete(TABLE_NAME, COLUMN_ID + "=" + id, null);

            success = true;

        } catch( SQLiteException ex ) {
            Log.d("DeliverableDAO", "Failed to delete the record");
            success = false;
        }
        return success;

    }

    public Cursor getDeliverablesCursor() {

        ArrayList<Deliverable> deliverables = new ArrayList<>();

        try {
            db = this.getReadableDatabase();
            //db.query(TABLE_NAME, new String[] {COLUMN_NAME, COLUMN_WEIGHT}, COLUMN_NAME+"=?", )
            cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
            cursor.moveToFirst();

         } catch( SQLiteException ex ) {
            Log.d("DeliverableDAO", "Failed to retrieve deliverables cursor");
        }


        return cursor;

    }

    public ArrayList<Deliverable> getDeliverables() {

        ArrayList<Deliverable> deliverables = new ArrayList<>();

        try {
            db = this.getReadableDatabase();
            //db.query(TABLE_NAME, new String[] {COLUMN_NAME, COLUMN_WEIGHT}, COLUMN_NAME+"=?", )
            cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);

            if(cursor.moveToFirst()) {
                do {
                    int deliverableId = cursor.getInt(0);
                    String deliverableName = cursor.getString(1);
                    String deliverableWeight = cursor.getString(2);
                    deliverables.add(new Deliverable(deliverableId, deliverableName, Double.parseDouble(deliverableWeight)));
                } while( cursor.moveToNext());
            }

        } catch( SQLiteException ex ) {
            Log.d("DeliverableDAO", "Failed to retrieve deliverables");
        }

        return deliverables;

    }

    public Deliverable getDeliverable(int id){

        Deliverable deliverable = null;
        db = this.getReadableDatabase();
        cursor =  db.rawQuery( "select * from " + TABLE_NAME + " where " + COLUMN_ID + "="+id+"", null );
        if(cursor.moveToFirst()) {

            deliverable = new Deliverable(cursor.getInt(0), cursor.getString(1), Double.parseDouble(cursor.getString(2)));

        }
        return deliverable;
    }

    public void closeConnection() {

        if( cursor != null) { cursor.close(); }
        if( db != null) { db.close(); }

    }


}

